﻿using UnityEngine;
using System.Collections;

public static class MathUtils {

	public static Vector2 GetRandomPointInRadius(float radius, Vector2 center)
	{
		float a = Random.Range(0.0f, 1.0f);
		float b = Random.Range(0.0f, 1.0f);
		
		if(b < a)
		{
			float temp = b;
			b = a;
			a = temp;
		}
		
		float x = b * radius * Mathf.Cos(2*Mathf.PI*a/b) + center.x;
		float y = b * radius * Mathf.Sin(2*Mathf.PI*a/b) + center.y;
		
		return new Vector2(x,y);
	}
}

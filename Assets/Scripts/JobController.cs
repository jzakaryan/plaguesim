﻿using System;
using System.Collections.Generic;
using System.Linq;

public sealed class JobController
{
	private static readonly JobController instance = new JobController();

	private List<IJob> jobs = new List<IJob>();

	// Explicit static constructor to tell C# compiler
	// not to mark type as beforefieldinit
	static JobController()
	{
	}
	
	private JobController()
	{
	}
	
	public static JobController Instance
	{
		get
		{
			return instance;
		}
	}

	public void AddJob(IJob newJob)
	{
		jobs.Add(newJob);
	}

	public void RemoveJob(IJob job)
	{
		jobs.Remove(job);
	}

	public void ExecuteJobsForCurrentTime()
	{
		var currentTimeJobs = jobs.Where(j => j.ScheduledTime == GameTimer.CurrentTime).ToList();

		foreach (var job in currentTimeJobs) 
		{
			job.Start();
			jobs.Remove(job);
		}
	}

	public int WaitingJobCount 
	{
		get
		{
			return jobs.Count;
		}
	}
}

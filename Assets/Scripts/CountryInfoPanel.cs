﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountryInfoPanel : MonoBehaviour {

	public Text InfectedCountText;
	public Text DeadCountText;
	public Text CountryNameText;

	public CountryInfo Country;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Country != null)
		{
			if(InfectedCountText != null)
			{
				InfectedCountText.text = Country.Infected.ToString();
			}

			if(DeadCountText != null)
			{
				DeadCountText.text = Country.Dead.ToString();
			}

			if(CountryNameText != null)
			{
				CountryNameText.text = Country.Name;
			}
		}
	}
}

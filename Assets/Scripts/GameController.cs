﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	public int jobCount = 10;

	void Awake()
	{
		InitializeGameTimer();
		CreateDummyJobs();
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		GameTimer.Tick();

		if(JobController.Instance.WaitingJobCount != 0)
		{
			Debug.Log("Current time : " + GameTimer.CurrentTime);
			JobController.Instance.ExecuteJobsForCurrentTime();
		}
	}

	private void InitializeGameTimer()
	{
		GameTimer.Start();
	}

	private void CreateDummyJobs()
	{
		for(int jobNum = 0; jobNum < jobCount; ++jobNum)
		{
			TestJob job = new TestJob();
			job.Schedule(GameTimer.CurrentTime.AddMinutes(2 + jobNum).AddSeconds(jobNum));

			JobController.Instance.AddJob(job);
		}
	}
}

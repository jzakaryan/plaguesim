﻿using System;

public static class GameTimer 
{
	private static int MinTick = 1;

	static GameTimer() 
	{
		IsRunning = false;
	}

	public static bool IsRunning { get; private set; }

	public static DateTime CurrentTime { get; private set; }

	public static void Start()
	{
		IsRunning = true;
		CurrentTime = DateTime.MinValue;
	}

	public static void Stop()
	{
		IsRunning = false;
	}

	public static void Resume()
	{
		IsRunning = true;
	}

	public static void Tick()
	{
		if(IsRunning)
		{
			CurrentTime = CurrentTime.AddSeconds(MinTick);
		}
	}
}

using System;
using UnityEngine;

public class TestJob : IJob
{
	public void Schedule(DateTime startTime)
	{
		ScheduledTime = startTime;
	}

	public void Start()
	{
		Debug.Log("Start called for job scheduled at : " + ScheduledTime);
	}

	public void Stop()
	{
		throw new NotImplementedException();
	}

	public void Pouse()
	{
		throw new NotImplementedException();
	}

	public DateTime ScheduledTime 
	{
		get; private set;
	}	
}


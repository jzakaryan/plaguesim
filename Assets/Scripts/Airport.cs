﻿using UnityEngine;
using System.Collections;

public class Airport : MonoBehaviour {

	private Animator animator;

	// Use this for initialization
	void Start () {
		this.animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Activate(float delay)
	{
		this.StartCoroutine(SetToActiveState(delay));
	}
	
	private IEnumerator SetToActiveState(float delay)
	{
		yield return new WaitForSeconds(delay);
		this.animator.SetTrigger("activate");
	}
		
	public void SendPlane(Airport destination)
	{		
		var airplaneObject = (GameObject)GameObject.Instantiate(Resources.Load("Airplane"));
		airplaneObject.AddComponent<LinearMovementController>();
		var movementController = airplaneObject.GetComponent<LinearMovementController>();
		movementController.StartPoint = this.transform.position;
		movementController.EndPoint = destination.transform.position;
		movementController.Smooth = 1.0f;
		movementController.DestroyOnArrival = true;
		movementController.StartMovement();
	}
}

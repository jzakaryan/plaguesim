﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldController : MonoBehaviour {
	private CountryInfoPanel infoPanel;

	public List<Country> Countries;
	private Country selectedCountry = null;
	private bool isCountrySelected = false;
	private bool isCountrySelectTime = false;

	void Awake()
	{
		GameTimer.Start();
	}

	// Use this for initialization
	void Start () {
		InitializeMap();
		infoPanel = GameObject.FindObjectOfType<CountryInfoPanel>();

		SelectCountry(gameObject.GetComponent<Country>());
		OpenCountrySelectionPopup();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Space))
		{
			this.SendRandomPlane();
		}	

		if(selectedCountry != null && infoPanel != null)
		{
			//Debug.Log(selectedCountryInfo.Infected);
			infoPanel.Country = selectedCountry.Info;
		}
	}

	void FixedUpdate()
	{
		GameTimer.Tick();
		
		if(JobController.Instance.WaitingJobCount != 0)
		{
			JobController.Instance.ExecuteJobsForCurrentTime();
		}
	}
	
	public GameObject CreateSprite (Texture2D tex, string name) {
		Texture2D _texture = tex;
		Sprite newSprite = Sprite.Create(_texture, new Rect(0f, 0f, _texture.width, _texture.height), new Vector2(0.5f, 0.5f),128f);
		newSprite.name = name;
		GameObject sprGameObj = new GameObject();
		sprGameObj.name = name;
		sprGameObj.AddComponent<SpriteRenderer>();
		SpriteRenderer sprRenderer = sprGameObj.GetComponent<SpriteRenderer>();
		sprRenderer.sprite = newSprite;
		return sprGameObj;
	}
	
	void InitializeMap()
	{
		// Africa
		var africa = GameObject.Find("Africa");
		africa.AddComponent<Country>();
		var africaScript = africa.GetComponent<Country>();
		AddAirportToCountry(africaScript, new Vector3{x = -0.76f, y = 0.47f});
		AddAirportToCountry(africaScript, new Vector3{x = 0.97f, y = -0.28f});
		AddAirportToCountry(africaScript, new Vector3{x = 1.79f, y = -1.67f});
		AddAirportToCountry(africaScript, new Vector3{x = 2.36f, y = 1.89f});
		africaScript.FormatedName = "Africa";
		Countries.Add(africaScript);
			
		// South America
		var southAmerica = GameObject.Find("SouthAmerica");
		southAmerica.AddComponent<Country>();
		var southAmericaScript = southAmerica.GetComponent<Country>();		
		AddAirportToCountry(southAmericaScript, new Vector3{x = -5.69f, y = 0.09f});
		AddAirportToCountry(southAmericaScript, new Vector3{x = -4.1f, y = -1.06f});
		AddAirportToCountry(southAmericaScript, new Vector3{x = -6.24f, y = -1.61f});
		AddAirportToCountry(southAmericaScript, new Vector3{x = -5.97f, y = -3.87f});
		southAmericaScript.FormatedName = "South America";
		Countries.Add(southAmericaScript);
		
		// Middle East
		var middleEast = GameObject.Find("MiddleEast");
		middleEast.AddComponent<Country>();
		var middleEastScript = middleEast.GetComponent<Country>();
		AddAirportToCountry(middleEastScript, new Vector3{x = 4.86f, y = 2.05f});
		middleEastScript.FormatedName = "Middle East";
		Countries.Add(middleEastScript);
		
		var asia = GameObject.Find("Asia");
		asia.AddComponent<Country>();
		var asiaScript = asia.GetComponent<Country>();
		AddAirportToCountry(asiaScript, new Vector3{x = 6.8f, y = 1.1f});
		AddAirportToCountry(asiaScript, new Vector3{x = 5.26f, y = 4.76f});
		AddAirportToCountry(asiaScript, new Vector3{x = 7.65f, y = 3.6f});
		AddAirportToCountry(asiaScript, new Vector3{x = 10.15f, y = 2.3f});
		asiaScript.FormatedName = "Asia";
		Countries.Add(asiaScript);
		
		var europe = GameObject.Find("Europe");
		europe.AddComponent<Country>();
		var europeScript = europe.GetComponent<Country>();
		AddAirportToCountry(europeScript, new Vector3{x = 1.31f, y = 3.65f});
		AddAirportToCountry(europeScript, new Vector3{x = -0.18f, y = 4.64f});
		europeScript.FormatedName = "Europe";
		Countries.Add(europeScript);
		
		var australia = GameObject.Find("Australia");
		australia.AddComponent<Country>();
		var australiaScript = australia.GetComponent<Country>();
		AddAirportToCountry(australiaScript, new Vector3{x = 12.82f, y = -3.27f});
		australiaScript.FormatedName = "Australia";
		Countries.Add(australiaScript);
		
		var northAmerica = GameObject.Find("NorthAmerica");
		northAmerica.AddComponent<Country>();
		var northAmericaScript = northAmerica.GetComponent<Country>();
		AddAirportToCountry(northAmericaScript, new Vector3{x = -7.39f, y = 2.53f});
		AddAirportToCountry(northAmericaScript, new Vector3{x = -10.82f, y = 3.24f});
		AddAirportToCountry(northAmericaScript, new Vector3{x = -10.21f, y = 5.23f});		
		AddAirportToCountry(northAmericaScript, new Vector3{x = -5.82f, y = 4.03f});	
		northAmericaScript.FormatedName = "North America";
		Countries.Add(northAmericaScript);
		
		ActivateAllAirports();
			
	}
	
	void AddAirportToCountry(Country country, Vector3 position)
	{
		var airportObject = (GameObject)GameObject.Instantiate(Resources.Load("Airport"));
		airportObject.transform.localPosition = position;
		airportObject.transform.parent = country.gameObject.transform;
		country.Airports.Add(airportObject);
	}
	
	void ActivateAllAirports()
	{
		float deltaDelay = 0.08f;
		float delay = deltaDelay;
		
		foreach(var country in this.Countries)
		{
			foreach(var airport in country.Airports)
			{
				airport.GetComponent<Airport>().Activate(delay);
				delay += deltaDelay;
			}
		}
	}
	
	void SendRandomPlane()
	{
 		int srcCountryIndex = Random.Range(0, Countries.Count);
		int destCountryIndex = Random.Range(0, Countries.Count);
		int srcAirportIndex = Random.Range(0, Countries[srcCountryIndex].Airports.Count);
		int destAirportIndex = Random.Range(0, Countries[destCountryIndex].Airports.Count);
		
		var srcAirport = Countries[srcCountryIndex].Airports[srcAirportIndex];
		var destAirport = Countries[destCountryIndex].Airports[destAirportIndex];

		PlaneScheduleJob job = new PlaneScheduleJob();
		job.Schedule(GameTimer.CurrentTime.AddSeconds(Random.Range(1, 10)));
		job.SourceAirport = srcAirport;
		job.DestinationAirport = destAirport;


		JobController.Instance.AddJob(job);

		//srcAirport.GetComponent<Airport>().SendPlane(destAirport.GetComponent<Airport>());
	}

	public void SelectCountry(Country country)
	{
		selectedCountry = country;

		if(isCountrySelectTime)
		{
			isCountrySelectTime = false;

			var panel = GameObject.FindObjectOfType<ModalPanel>();
			
			if(panel != null)
			{
				panel.Choice(string.Format("Are you sure you want to start from '{0}'?", country.FormatedName), 
				             () => { isCountrySelectTime = false; isCountrySelected = true; country.Infect();},
				() => { isCountrySelectTime = true; isCountrySelected = false; });
			}
		}
	}

	private void OpenCountrySelectionPopup()
	{
		var panel = GameObject.FindObjectOfType<ModalPanel>();

		if(panel != null)
		{
			panel.Choice("Please select country to start", () => { isCountrySelectTime = true; });
		}
	}
}

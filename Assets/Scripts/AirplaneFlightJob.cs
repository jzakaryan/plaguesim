using System;
using UnityEngine;

public class AirplaneFlightJob : IJob
{

	public Airport SourceAirport {get; set;}
	
	public Airport DestinationAirport {get; set;}
	
	public void Schedule(DateTime startTime)
	{
		ScheduledTime = startTime;
	}
	
	public void Schedule(DateTime startTime, Airport srcAirport, Airport destAirport)
	{
		this.SourceAirport = srcAirport;
		this.DestinationAirport = destAirport;
		this.ScheduledTime = ScheduledTime;
	}

	public void Start()
	{
		Debug.Log("Start called for flight job scheduled at : " + ScheduledTime);
		
		SourceAirport.GetComponent<Airport>().SendPlane(DestinationAirport.GetComponent<Airport>());
	}

	public void Stop()
	{
		throw new NotImplementedException();
	}

	public void Pouse()
	{
		throw new NotImplementedException();
	}

	public DateTime ScheduledTime 
	{
		get; private set;
	}	
}


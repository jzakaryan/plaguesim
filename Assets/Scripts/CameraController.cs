﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	private float cameraSpeed = 5f;
	private float scrollEdge = 0.05f;
	
	private Vector3 boundaryTopLeft = new Vector3 {x = -6f, y = 2.57f};
	private Vector3 boundaryBottomRight = new Vector3 {x = 6.03f, y = -2.26f};
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if((Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width * (1 - this.scrollEdge)) 
			&& this.transform.position.x < boundaryBottomRight.x)
		{
			transform.Translate(Vector3.right * Time.deltaTime * this.cameraSpeed, Space.World);
		}
		
		else if((Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= Screen.width * this.scrollEdge) 
			&& this.transform.position.x > boundaryTopLeft.x)
		{			
			transform.Translate(Vector3.left * Time.deltaTime * this.cameraSpeed, Space.World);			
		}
		
		else if((Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height * (1 - this.scrollEdge))
			&& this.transform.position.y < boundaryTopLeft.y)
		{
			transform.Translate(Vector3.up * Time.deltaTime * this.cameraSpeed, Space.World);
		}
		
		else if((Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= Screen.height * this.scrollEdge)
			&& this.transform.position.y > boundaryBottomRight.y)
		{
			transform.Translate(Vector3.down * Time.deltaTime * this.cameraSpeed, Space.World);
		}
		
		else if(Input.GetAxis("Mouse ScrollWheel") > 0)
		{
			Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize-0.2f, 3);
		}
		
		else if(Input.GetAxis("Mouse ScrollWheel") < 0)
		{
			Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize+0.2f, 5);
		}
}
}

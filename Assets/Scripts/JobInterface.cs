﻿using System;

public interface IJob
{
	void Schedule(DateTime startTime);
	void Start();
	void Stop();
	void Pouse();
	DateTime ScheduledTime {get; }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Linq;

public class Country : MonoBehaviour {
	private WorldController worldController;
	private List<Vector2> infectionPoints = new List<Vector2>();
	private Animator animator;
	private bool isWorld = false;

	public Country()
	{
		Airports = new List<GameObject>();
		Seaports = new List<GameObject>();
	}

	public List<GameObject> Airports { get; set; }
	
	public List<GameObject> Seaports { get; set; }

	public string FormatedName;

	public CountryInfo Info;
	
	public Transform CenterMarker;

	// Use this for initialization
	void Start () {		
		isWorld = tag == "GameController";
		animator = this.GetComponent<Animator>();
		worldController = GameObject.FindObjectOfType<WorldController>();
		InitializeCountryInfo();
		this.CenterMarker = transform.Find("Center");				
		
		if(!isWorld)
		{
			this.InfectRandomPointOnCountry();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Info != null && Info.Infected != 0)
		{
			// flight must be generated based on infected population
			bool generateFlight = Random.Range(0, 300) == 1;

			if(generateFlight)
			{
				GameObject destAirport = null;
				GameObject srcAirport = null;

				// get source airport from available list
				if(Airports != null && Airports.Count > 0)
				{
					int srcAirportIndex = Random.Range(0, Airports.Count);
					srcAirport = Airports[srcAirportIndex]; 
								
					// get destination airport from world controller
					if(worldController != null)
					{
						Country destCountry = null;
						do
						{
							destCountry = worldController.Countries[Random.Range(0, worldController.Countries.Count)];
						}
						while(destCountry.name == this.name);

						int destAirportIndex = Random.Range(0, destCountry.Airports.Count);
						destAirport = destCountry.Airports[destAirportIndex];
					}

					// schedule flight
					if(destAirport != null && srcAirport != null)
					{
						PlaneScheduleJob job = new PlaneScheduleJob();
						job.Schedule(GameTimer.CurrentTime.AddSeconds(Random.Range(1, 10)));
						job.SourceAirport = srcAirport;
						job.DestinationAirport = destAirport;
						
						
						JobController.Instance.AddJob(job);
					}
				}
			}
		}

		if(isWorld)
		{
			UpdateWorldContryInfo();
		}
	}

	void OnMouseUp() 
	{
		if(!enabled)
		{
			return;
		}

		Debug.Log ("Down : " + gameObject.name);
		if(isWorld)
		{
			UpdateWorldContryInfo();
		}

		worldController.SelectCountry(this);
		
		if(animator != null)
		{
			this.animator.SetTrigger("Pulse");
		}
	}

	private void InitializeCountryInfo()
	{
		if(isWorld)
		{
			UpdateWorldContryInfo();
		}
		else
		{
			Info = new CountryInfo
			{
				Infected = (int)Random.Range(1000f, 100000f),
				Dead = (int)Random.Range(100f, 2000f),
				Name = FormatedName
			};
		}
	}

	private void UpdateWorldContryInfo()
	{
		Info = new CountryInfo
		{
			Name = name
		};

		if(worldController != null && worldController.Countries != null)
		{
			var otherContries = worldController.Countries.Where(c => c.name != this.name && c.Info != null);

			Info.Infected = otherContries.Sum(c => c.Info.Infected);
			Info.Dead = otherContries.Sum(c => c.Info.Dead);

			//worldController.SelectCountry(Info);
		}
	}

	public void Infect()
	{
		Debug.Log(this.FormatedName + " is infected");
	}
	
	public void InfectRandomPointOnCountry()
	{
		Vector2 initialPoint;
		
		if(infectionPoints != null && infectionPoints.Count == 0)
		{
			initialPoint = new Vector2(CenterMarker.position.x, CenterMarker.position.y);
		}
		else
		{
			int randomIndex = Random.Range(0, infectionPoints.Count);
			initialPoint = infectionPoints[randomIndex];
		}
		
		var newInfectionPoint = MathUtils.GetRandomPointInRadius(0.1f, initialPoint);
		
		// check to see if the point is on the country
		var hit = Physics2D.Raycast(newInfectionPoint, Vector2.zero);
		var collider = this.gameObject.GetComponent<PolygonCollider2D>();
		if(hit.collider != collider)
		{
			return;
		}
		
		//var textureBlenderScript = this.gameObject.GetComponent<TextureBlender>();
		//textureBlenderScript.BlendTexture(newInfectionPoint);
		
		AddInfectionPoint(newInfectionPoint);
	}
	
	public void AddInfectionPoint(Vector2 position)		
	{
		var airportObject = (GameObject)GameObject.Instantiate(Resources.Load("InfectionPoint"));
		airportObject.transform.localPosition = position;
		airportObject.transform.parent = transform;
		infectionPoints.Add(position);
	}
}

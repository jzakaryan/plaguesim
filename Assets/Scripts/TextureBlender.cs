﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureBlender : MonoBehaviour {

	/// <summary>
	/// Gets or sets the soft brush texture that is used for blending.
	/// </summary>
	/// <value>The brush texture.</value>
	public Texture2D brushTexture;
	
	private List<Vector2> infectionPoints;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void BlendTexture(Vector2 coordinates)
	{
		int x = (int)coordinates.x;
		int y = (int)coordinates.y;
		
		float brushWidth = brushTexture.width;
		float brushHeight = brushTexture.height;
		
		var spriteRenderer = (SpriteRenderer)this.gameObject.GetComponent<SpriteRenderer>();
		var mainTexture = spriteRenderer.sprite.texture;
		
		if(x + brushWidth > mainTexture.width || y + brushHeight > mainTexture.height)
		{
			Debug.LogWarning("Couldn't blend texture. Part of brush texture is out of bounds of main texture.");
		}
		
		for(int tmpY = y; tmpY < y + brushHeight; tmpY++)
		{
			for(int tmpX = x; tmpX < x + brushWidth; tmpX++)
			{
				Color mainColor = mainTexture.GetPixel(tmpX, tmpY);
				Color brushColor = brushTexture.GetPixel(tmpX, tmpY);
				Color result = new Color();
				
				//if(brushColor.a != 0.0f && mainColor.a != 0.0f)
				//{
					result.r = 1.0f;// (brushColor.r * brushColor.a) + (mainColor.r * (1.0f - brushColor.r));
					result.g = 0.0f;// (brushColor.g * brushColor.g) + (mainColor.g * (1.0f - brushColor.g));
					result.r = 0.0f;// (brushColor.b * brushColor.b) + (mainColor.b * (1.0f - brushColor.b));
					result.a = 1.0f;// Mathf.Min(1.0f, brushColor.a + mainColor.a);
					mainTexture.SetPixel(tmpX, tmpY, result);
				//}				
			}
		}
		
		mainTexture.Apply();
		gameObject.GetComponent<SpriteRenderer>().material.mainTexture = mainTexture;
		//infectionPoints.Add(coordinates);
	}
}

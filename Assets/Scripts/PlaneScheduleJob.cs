using System;
using UnityEngine;

public class PlaneScheduleJob : IJob
{
	public void Schedule (DateTime startTime)
	{
		ScheduledTime = startTime;
	}

	public void Start ()
	{
		SourceAirport.GetComponent<Airport>().SendPlane(DestinationAirport.GetComponent<Airport>());
	}

	public void Stop ()
	{
		throw new NotImplementedException ();
	}

	public void Pouse ()
	{
		throw new NotImplementedException ();
	}

	public DateTime ScheduledTime { get; private set; }

	public GameObject SourceAirport { get; set; }
	public GameObject DestinationAirport { get; set; }
}


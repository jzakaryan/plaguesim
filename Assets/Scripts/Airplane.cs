﻿using UnityEngine;
using System.Collections;

public class Airplane : MonoBehaviour {

	public bool IsInTransit = false;
	private Animator animator;

	// Use this for initialization
	void Start () {
		animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(IsInTransit)
		{
			PositionChanging();	
		}
	}
	
	void PositionChanging()
	{
		
	}
	
	public void Land()
	{
		var animator = this.gameObject.GetComponent<Animator>();
		animator.SetTrigger("Land");
	}
}

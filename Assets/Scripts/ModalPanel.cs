﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

//  This script will be updated in Part 2 of this 2 part series.
public class ModalPanel : MonoBehaviour {
	
	public Text question;
	public Button yesButton;
	public Button noButton;
	public Button cancelButton;
	public Button okButton;
	public GameObject modalPanelObject;
	
	private static ModalPanel modalPanel;	
	
	public static ModalPanel Instance () {
		if (!modalPanel) 
		{
			modalPanel = FindObjectOfType(typeof (ModalPanel)) as ModalPanel;
			if (!modalPanel)
			{
				Debug.LogError ("There needs to be one active ModalPanel script on a GameObject in your scene.");
			}
		}
		
		return modalPanel;
	}
	
	// Ok: A string, a Yes event and OK event
	public void Choice (string question, UnityAction okEvent) {
		BlockCountries();
		modalPanelObject.SetActive (true);	
		
		okButton.onClick.RemoveAllListeners();
		okButton.onClick.AddListener (okEvent);
		okButton.onClick.AddListener (ClosePanel);
		
		this.question.text = question;
		
		yesButton.gameObject.SetActive (false);
		noButton.gameObject.SetActive (false);
		cancelButton.gameObject.SetActive (false);
		okButton.gameObject.SetActive (true);
	}
	
	// Yes/No/Cancel/Ok: A string, a Yes event, a No event, Cancel event and OK event
	public void Choice (string question, UnityAction yesEvent, UnityAction noEvent, UnityAction cancelEvent, UnityAction okEvent) {
		BlockCountries();
		modalPanelObject.SetActive (true);
		
		yesButton.onClick.RemoveAllListeners();
		yesButton.onClick.AddListener (yesEvent);
		yesButton.onClick.AddListener (ClosePanel);

		noButton.onClick.RemoveAllListeners();
		noButton.onClick.AddListener (noEvent);
		noButton.onClick.AddListener (ClosePanel);
		
		cancelButton.onClick.RemoveAllListeners();
		cancelButton.onClick.AddListener (cancelEvent);
		cancelButton.onClick.AddListener (ClosePanel);

		okButton.onClick.RemoveAllListeners();
		okButton.onClick.AddListener (okEvent);
		okButton.onClick.AddListener (ClosePanel);
		
		this.question.text = question;

		yesButton.gameObject.SetActive (true);
		noButton.gameObject.SetActive (true);
		cancelButton.gameObject.SetActive (true);
		okButton.gameObject.SetActive (true);
	}

	// Yes/No A string, a Yes event and a No event
	public void Choice (string question, UnityAction yesEvent, UnityAction noEvent) {
		BlockCountries();
		modalPanelObject.SetActive (true);
		
		yesButton.onClick.RemoveAllListeners();
		yesButton.onClick.AddListener (yesEvent);
		yesButton.onClick.AddListener (ClosePanel);
		
		noButton.onClick.RemoveAllListeners();
		noButton.onClick.AddListener (noEvent);
		noButton.onClick.AddListener (ClosePanel);
		
		this.question.text = question;
		
		yesButton.gameObject.SetActive (true);
		noButton.gameObject.SetActive (true);
		cancelButton.gameObject.SetActive (false);
		okButton.gameObject.SetActive (false);
	}
	
	void ClosePanel () {
		modalPanelObject.SetActive (false);
		ResetCountries();
	}

	void BlockCountries()
	{
		SetCountryState(false);
	}

	void ResetCountries()
	{
		SetCountryState(true);
	}

	void SetCountryState(bool isEnabled)
	{
		var countries = GameObject.FindObjectsOfType<Country>();
		
		if(countries != null)
		{
			foreach (var country in countries) 
			{
				country.enabled = isEnabled;
			}
		}
	}
}
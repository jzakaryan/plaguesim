﻿using UnityEngine;
using System.Collections;

public class LinearMovementController : MonoBehaviour {

	float distance;
	
	bool movementStarted = false;
	
	public Vector3 StartPoint {get; set;}
	
	public Vector3 EndPoint {get; set;}
	
	public float Smooth {get; set;}
	
	public bool DestroyOnArrival {get; set;}
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	public void StartMovement()
	{
		this.movementStarted = true;
		this.distance = Vector3.Distance(EndPoint, StartPoint);
				
		Vector3 direction = EndPoint - StartPoint;
						
		float angle = Vector3.Angle(Vector3.up, direction);
		
		if(EndPoint.x > StartPoint.x)
		{
			angle = -angle;
		}
		
		this.transform.Rotate(new Vector3(0.0f, 0.0f, angle));
	}
	
	// Update is called once per frame
	void Update () {	
		if(movementStarted)
		{		
		
			if((transform.position == EndPoint) && DestroyOnArrival)
			{
				GameObject.Destroy(this.gameObject);
			}
		
			UpdatePosition();
		}
	}
	
	void UpdatePosition()
	{	
		transform.position = Vector3.MoveTowards(StartPoint, EndPoint, Smooth * UnityEngine.Time.deltaTime);
		StartPoint = transform.position;
		
		float remainingDistance = Vector3.Distance(StartPoint, EndPoint);
		float remainingTime = remainingDistance / Smooth;
		
		if(remainingTime < 1.7f)
		{
			var airplaneScript = this.gameObject.GetComponent<Airplane>();
			if(airplaneScript != null)
			{
				airplaneScript.Land();
			}
		}
	}
}
